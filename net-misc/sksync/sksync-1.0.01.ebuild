# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"
inherit eutils java-pkg-2
DESCRIPTION="Java based server for syncing android phones"
HOMEPAGE="http://sktechnology.net/android/sksync/"
MY_P="syncserver.jar"
SRC_URI="http://sktechnology.net/android/sksync/files/${MY_P}"

LICENSE=""
SLOT="0"

KEYWORDS="~x86 ~amd64"

IUSE=""

#DEPEND=""

# Run-time dependencies. Must be defined to whatever this depends on to run.
# The below is valid if the same run-time depends are required to compile.
RDEPEND="${DEPEND}
		 =virtual/jre-1.6.0"


src_install() {
		 java-pkg_newjar ${MY_P}
}
