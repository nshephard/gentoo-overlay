# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
#PYTHON_COMPAT=


DESCRIPTION="Application for managing Onlykey password device."
HOMEPAGE="https://github.com/trustcrypto/OnlyKey-App"
SRC_URI="https://github.com/trustcrypto/${PN}/archive/refs/tags/v${PV}.tar.gz"
KEYWORDS="~amd64"
LICENSE=""
SLOT="0"
IUSE=""
BDEPEND="net-libs/nodejs"

src_compile() {
	# nothing to compile here
	:
}

src_install() {
	npm \
		--audit false \
		--color false \
		--foreground-scripts \
		--global \
		--offline \
		--omit dev \
		--prefix "${ED}"/usr \
		--progress false \
		--verbose \
		install "${DISTDIR}/${P}".tgz || die "npm install failed"

	einstalldocs
}
