# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="Linkage and Association testing of Quantitative Traits in.\
nuclear families."
HOMEPAGE="http://www.sph.umich.edu/csg/abecasis/qtdt/"
SRC_URI="http://www.sph.umich.edu/csg/abecasis/qtdt/download/${P}.tar.gz"
SLOT="0"

# License of the package.  This must match the name of file(s) in
# /usr/portage/licenses/.  For complex license combination see the developer
# docs on gentoo.org for details.
LICENSE=""

KEYWORDS="~x86"

src_install() {
        mkdir ${D}usr #For some reason this is needed
        make install INSTALLDIR=${D}usr/bin || die
}
