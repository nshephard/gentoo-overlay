# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

# Redefine package name to reflect use of Upper/Lower case
MY_P="CaTS-${PV}"

DESCRIPTION="CaTS is a power calculator for large-scale genetic association studies"
HOMEPAGE="http://www.sph.umich.edu/csg/abecasis/CaTS/"
SRC_URI="http://www.sph.umich.edu/csg/abecasis/CaTS/${MY_P}.tar.gz"

LICENSE="as-is"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

S=${WORKDIR}/${MY_P}

src_compile() {
	emake all || die "emake all"
}

src_install() {
	mkdir ${D}usr
	emake INSTALLDIR=${D}usr/bin install  || die "emake install"
} 
