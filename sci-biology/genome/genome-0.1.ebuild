# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="genome is a program for rapid coalescent-based whole genome simulation."
HOMEPAGE="http://www.sph.umich.edu/csg/liang/genome/"
SRC_URI="http://www.sph.umich.edu/csg/liang/genome/${P}.tar.gz"
SLOT="0"

# License of the package.  This must match the name of file(s) in
# /usr/portage/licenses/.  For complex license combination see the developer
# docs on gentoo.org for details.
LICENSE=""

KEYWORDS="~x86"

src_install() {
	dobin genome
}
