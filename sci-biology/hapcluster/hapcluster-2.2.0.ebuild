# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="HapCluster++ is a software package for linkage disequilibrium\
 mapping based on a Bayesian Markov-chain Monte Carlo (MCMC) method for\
 fine-scale linkage-disequilibrium gene mapping using high-density marker maps."
HOMEPAGE="http://www.daimi.au.dk/~mailund/HapCluster/"
SRC_URI="http://www.daimi.au.dk/~mailund/HapCluster/download/${P}.tar.gz"
SLOT="0"

LICENSE="/usr/portage/licenses/GPL-2"

KEYWORDS="~x86"

DEPEND="dev-libs/boost
	sci-biology/snpfile
	sci-libs/gsl"
RDEPEND="${DEPEND}"

src_install() {
        make install DESTDIR=${D}|| die
}
