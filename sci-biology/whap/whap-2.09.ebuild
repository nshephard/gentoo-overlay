# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="whap is a program for testing SNP haplotype associations with\
  qualitatitve and quantitative traits in samples with or without parental\
  information."
HOMEPAGE="http://pngu.mgh.harvard.edu/~purcell/whap/"
SRC_URI="http://pngu.mgh.harvard.edu/~purcell/dist/whap-src-2.09.tar.gz"
SLOT="0"

# License of the package.  This must match the name of file(s) in
# /usr/portage/licenses/.  For complex license combination see the developer
# docs on gentoo.org for details.
LICENSE="GPL-2"

#
DEPEND="sci-biology/snphap"

KEYWORDS="~x86"

src_install() {
#	make install DESTDIR=${D}|| die
	dobin whap || die
}
