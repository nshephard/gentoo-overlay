# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="Genetic association analysis of SNPs and haplotypes in families\
and population based studies."
HOMEPAGE="http://www.mrc-bsu.cam.ac.uk/personal/frank/software/unphased/"
SRC_URI="http://www.mrc-bsu.cam.ac.uk/personal/frank/software/unphased/${P}.tar.gz"
SLOT="0"
LICENSE="GPL-2"

KEYWORDS="~x86"


src_install() {
        dobin unphased
}
