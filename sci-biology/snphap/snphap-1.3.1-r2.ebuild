# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="Hapltoype frequency estimation via EM-algorithm."
HOMEPAGE="http://www-gene.cimr.cam.ac.uk/clayton/software/"
SRC_URI="http://www-gene.cimr.cam.ac.uk/clayton/software/${P}.tar.gz"
SLOT="0"
LICENSE="Artistic"

KEYWORDS="~x86"


src_install() {
        dobin snphap
}
